#! /usr/bin/perl

# Copyright: 2013 Nobuhiro iwamatsu <iwamatsu@debian.org>
# License: MIT/X
#

use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

insert_before("dh_gencontrol", "dh_rebar");

add_command_options("dh_compress", "-X.erl -X.beam");

1;
